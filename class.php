<?php
    if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
        die();

    //use \Bitrix\Main\Localization\Loc;
    //Loc::loadMessages(__FILE__);
    //CBitrixComponent::includeComponentClass("bitrix:tasks.base");

    //    $APPLICATION->IncludeComponent(
    //        "uu:city.user.assign",
    //        ".default",
    //        [
    //            'IBLOCK_ID' => 50
    //        ]
    //    );

    /**
     * Component Class
     */
    class CityUserAssignComponent extends \CBitrixComponent
    {
        //public function __construct( $component = null ){parent::__construct( $component );}

         /**
          * Запуск компонента
          * @return array|mixed|null
          */
        public function executeComponent()
        {
            $this->arResult = [];

            $this->getOpenLines();
            $this->getColumns();
            $this->getRows();
            $this->includeComponentTemplate();

            //echo '<pre>'; print_r(  ); echo '</pre>';

            return $this->arResult;
        }

         /**
          * Получает список открытых линий
          * @param array $array
          */
        public function getOpenLines( $array = [] )
        {
            if ( $this->arResult['OPEN_LINES'] && !$array )
                return $this->arResult['OPEN_LINES'];

            $db = \Bitrix\ImOpenLines\Model\ConfigTable::getList([

                //'filter' => [],
                'limit' => 1000,
                'select' => ['ID', 'ACTIVE', 'LINE_NAME']

            ]);

            while ( $list = $db->Fetch() )
                $this->arResult['OPEN_LINES'][ $list['ID'] ] = $list['LINE_NAME'];

            $this->arResult['SELECT']['OPEN_LINES'] = $this->getHtml('openLinesSelect');

            return $this->arResult['OPEN_LINES'];

        }

         /**
          * Список колонок таблицы
          * @return mixed
          */
        public function getColumns()
        {
            if ( $this->arResult['COLUMNS'] )
                return $this->arResult['COLUMNS'];

            $this->arResult['COLUMNS'] = [

                [ 'id' => 'ID', 'name' => 'ID', 'sort' => 'ID', 'default' => true ],
                [ 'id' => 'NAME', 'name' => 'Город', 'sort' => 'NAME', 'default' => true ],
                [ 'id' => 'PROPERTY_OPEN_LINES', 'name' => 'Открытые линии', 'function' => 'getOpenLines', 'default' => true ]

            ];

            return $this->arResult['COLUMNS'];
        }

         /**
          * Получает поля
          * @return mixed
          */
        public function getSelectFields()
        {
            if ( $this->arResult['FIELDS'] )
                return $this->arResult['FIELDS'];

            if ( !$this->arResult['COLUMNS'] )
                return $this->getColumns();

            foreach ( $this->arResult['COLUMNS'] as $value )
                $this->arResult['FIELDS'][] = $value['id'];

            return $this->arResult['FIELDS'];
        }

         /**
          * Получает список городов
          * @return mixed
          */
        public function getRows()
        {
            if ( $this->arResult['ROWS'] )
                return $this->arResult['ROWS'];

            $db = \CIBlockElement::GetList(
                [ 'TIMESTAMP_X' => 'DESC', 'NAME' => 'ASC' ],
                [
                    'IBLOCK_ID' => $this->arParams['IBLOCK_ID'] ? : 50
                ],
                false,
                false,
                $this->getSelectFields()
            );

            while ( $list = $db->Fetch() )
            {
                // todo сменить поддержку открытых линий со старого формата на новый
                if ( $list['PROPERTY_OPEN_LINES_VALUE'] )
                    $list['PROPERTY_OPEN_LINES'] = $this->getHtml('openLines', $list['PROPERTY_OPEN_LINES_VALUE']);

                $this->arResult['ROWS'][ $list['ID'] ] = [

                    'data' => $list,
                    'actions' => [
                        [
                            'text'    => 'Редактировать',
                            'onclick' => 'City.prototype.action({ action: "edit", id: '.$list['ID'].', data: '. json_encode( $list) .'});',
                        ],
                        [
                            'text'    => 'Активировать',
                            'onclick' => 'City.prototype.action({ action: "enable", id: '.$list['ID'].'});'
                        ],
                        [
                            'text'    => 'Отключить',
                            'onclick' => 'City.prototype.action({ action: "disable", id: '.$list['ID'].'});'
                        ],
                        [
                            'text'    => 'Удалить',
                            'onclick' => 'City.prototype.action({ action: "delete", id: '.$list['ID'].'});'
                        ]
                    ],
                ];
            }

            return $this->arResult['ROWS'];
        }

         /**
          *
          * @param null $command
          * @param array $array
          * @return string
          */
        public function getHtml( $command = null, $array = [] )
        {
            $result = [];

            if ( !$this->arResult['OPEN_LINES'] )
                $this->getOpenLines();

            if ( $command === 'openLines' )
            {
                foreach( $array as $id )
                {
                    //$click = 'BX.SidePanel.Instance.open(\'/services/openlines/connector/?ID=LINE='.$id.'\')';
                    //$result[] = '<div class="ui tiny label">';
                    $result[] = '<div>' . $this->arResult['OPEN_LINES'][ $id ] ? : $id . '</div>';
                    //$result[] = '</div>';
                }
            }

            if ( $command === 'openLinesSelect' )
            {
                foreach ( $this->arResult['OPEN_LINES'] as $id => $name )
                {
                    $result[] = '<div class="item" data-value="' . $id . '">' . $name . '</div>';
                }
            }

            return implode( '', $result);
        }

    }