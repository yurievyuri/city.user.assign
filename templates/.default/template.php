<?php
    if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

    /** Developed By Yuriev Yuri 17.06.2020 */
    /** @var array $arParams */
    /** @var array $arResult */
    /** @global CMain $APPLICATION */
    /** @global CUser $USER */
    /** @global CDatabase $DB */
    /** @var CBitrixComponentTemplate $this */
    /** @var string $templateName */
    /** @var string $templateFile */
    /** @var string $templateFolder */
    /** @var string $componentPath */
    /** @var CBitrixComponent $component */

    //$this->setFrameMode(true);

    \Bitrix\Main\Page\Asset::getInstance()->addCss( '/local/assets/semantic/semantic.css' );
    \Bitrix\Main\Page\Asset::getInstance()->addJs( '/local/assets/jquery/jquery-3.5.1.js' );
    //\Bitrix\Main\Page\Asset::getInstance()->addJs( '/local/assets/semantic/semantic.js' );

    CJSCore::Init(['popup', 'ui', 'alerts']);

    \Bitrix\Main\Loader::includeModule("ui");
    \Bitrix\Main\UI\Extension::load("ui.alerts");
    \Bitrix\Main\UI\Extension::load("ui.dialogs.messagebox");

    ?>
    <script src="/local/assets/semantic/semantic.js"></script>

    <div class="ui basic segment" style="margin: 0; padding: 0 1em;">

        <form class="ui form" id="cityUserAssignForm" style="display: block;">

            <input type="hidden" name="SAVED" value="N">
            <input type="hidden" name="component" value="<?=$component->getName();?>">
            <input type="hidden" name="componentPath" value="<?=$componentPath;?>/">
            <?foreach($arParams as $id => $value) echo '<input type="hidden" name="'.$id.'" value="'.$value.'">';?>

            <input type="hidden" name="MAIN[<?=$arParams['IBLOCK_ID']?>][ID]" value="" />

            <h3 class="ui block header">
                <i class="settings icon"></i>
                <div class="content">Настройки
                    <div class="sub header">Панель редактирования</div>
                </div>
            </h3>

            <div class="fields">

                <div class="sixteen wide field">
                    <label>Наименование:</label>
                    <input type="text" placeholder="NAME">
                </div>

            </div>


            <h3 class="ui block header">
                <i class="plug icon"></i>
                <div class="content">Открытые линии</div>
            </h3>

            <div class="fields">

                <div class="sixteen wide field">

                    <label>Список каналов:</label>

                    <div class="ui fluid search selection multiple dropdown">
                        <input type="hidden" name="MAIN[<?=$arParams['IBLOCK_ID']?>][PROPERTY_OPEN_LINES]">
                        <i class="dropdown icon"></i>
                        <div class="default text">выбрать</div>
                        <div class="menu"><?=$arResult['SELECT']['OPEN_LINES'];?></div>
                    </div>

                </div>

            </div>

            <h3 class="ui block header">
                <i class="calendar alternate icon"></i>
                <div class="content">Графики смен</div>
            </h3>


            <div class="ui success warning error message">
                <div class="header">Action Forbidden</div>
                <p>You can only sign up for an account once with a given e-mail address.</p>
            </div>

            <div id="cityUserAssignFormAlert" style="padding: 20px 0 10px;">

                <!--div class="ui-alert ui-alert-success ui-alert-sm ui-alert-icon-info ui-alert-close-animate"
                     style="margin-bottom:0;padding:32px 34px 31px 18px; margin: 25px 0;">
                    <span class="ui-alert-message">test</span>
                    <span class="ui-alert-close-btn"></span>
                </div-->

            </div>

            <button class="ui-btn ui-btn-success" data-type="submit" type="submit">Сохранить</button>
            <button class="ui-btn ui-btn-primary-dark" data-type="reset" type="reset">Очистить</button>
            <button class="ui-btn ui-btn-default" data-type="close">Закрыть</button>

        </form>

        <?php
        $APPLICATION->IncludeComponent(
            'bitrix:main.ui.grid',
            '',
            [
                'GRID_ID' => $arParams['GRID_ID'],
                //'NAV_OBJECT' => $nav,
                'ROWS' => $arResult['ROWS'],
                'COLUMNS' => $arResult['COLUMNS'],
                'AJAX_MODE' => 'Y',
                'AJAX_ID' => \CAjax::getComponentID('bitrix:main.ui.grid', '.default', $arParams['GRID_ID']),
                'PAGE_SIZES' => [],
                'SHOW_ROW_CHECKBOXES'       => false,
                'AJAX_OPTION_JUMP'          => 'N',
                'SHOW_CHECK_ALL_CHECKBOXES' => false,
                'SHOW_ROW_ACTIONS_MENU'     => true,
                'SHOW_GRID_SETTINGS_MENU'   => true,
                'SHOW_NAVIGATION_PANEL'     => false,
                'SHOW_PAGINATION'           => false,
                'SHOW_SELECTED_COUNTER'     => false,
                'SHOW_TOTAL_COUNTER'        => false,
                'SHOW_PAGESIZE'             => false,
                'SHOW_ACTION_PANEL'         => true,
                'ALLOW_COLUMNS_SORT'        => true,
                'ALLOW_COLUMNS_RESIZE'      => true,
                'ALLOW_HORIZONTAL_SCROLL'   => true,
                'ALLOW_SORT'                => true,
                'ALLOW_PIN_HEADER'          => true,
                'AJAX_OPTION_HISTORY'       => 'Y',
                //'TOTAL_ROWS_COUNT'          => $total,
                //'CURRENT_PAGE' => $nav->getCurrentPage()
            ]
        );?>

    </div>