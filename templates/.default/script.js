document.addEventListener("DOMContentLoaded", ()=> { City.prototype.init() });
let City = function(){};
City.prototype = {

    cache : null,
    id : null,
    classes : null,
    settings : null,
    element: null,
    button: null,
    error: null,

    /**
     * Инициализация входных параметров
     * @returns {boolean}
     */
    init : function()
    {
        if ( !BX )
        {
            this.error.critical = true;
            this.warning('Failed to get [BX] object to continue.');
        }

        this.settings = {

            tableId : 'cityUserAssignTable',
            formId: 'cityUserAssignForm',
            formAlertId: 'cityUserAssignFormAlert',
            component : document.querySelector( '[name="component"]' ).value || 'city.user.assign',
            url : document.querySelector( '[name="componentPath"]' ).value || '/local/components/uu/city.user.assign/',
            file : 'ajax.php',
            saved : document.querySelector( '[name="SAVED"]' ).value === 'Y',
            iblockId : document.querySelector( '[name="IBLOCK_ID"]' ).value || 50,
            timeOut : 500,
            debug: true
        };

        this.classes = {

            loader : 'loading',
            button : 'ui-btn',
            clock : 'ui-btn-clock',
            disabled : 'ui-btn-disabled'
        };

        this.element = {

            table : document.getElementById( this.settings.tableId ),
            form  : document.getElementById( this.settings.formId ),
            formAlert: document.getElementById( this.settings.formAlertId ),
            grid : BX.Main.gridManager.getById( this.settings.tableId ),
            popUpDialog : false
        };

        this.buttons = {

            formButtons : this.element.form && this.element.form.querySelectorAll('.ui-btn'),
            formSubmit : this.element.form && this.element.form.querySelector('[data-type="submit"]'),
            formClose : this.element.form && this.element.form.querySelector('[data-type="close"]'),
            currentActionButton : false

        }

        this.error = {

            critical : false,
            warning : []

        };

        this.semanticInit();
        this.eventsInit();

        if ( this.checkError() ) return false;

        console.log( '[ Remote Control Method is Started ]' );

    },

    /**
     * Подключение прослушок событий
     */
    eventsInit: function()
    {
        // обслуживание кликов на кнопки основной формы
        Array.from( this.buttons.formButtons ).map(( item )=> {

            if ( !item ) return;
            item.addEventListener('click', function( event )
            {
                City.prototype.buttons.currentActionButton = false;

                event.preventDefault();
                event.stopPropagation();

                if ( !item.getAttribute('data-type') ) return;

                City.prototype.buttons.currentActionButton = this;

                switch (item.getAttribute('data-type')) {

                    case 'submit' :

                        City.prototype.form.submit();

                        break;

                    case 'reset' :

                        City.prototype.form.reset();

                        break;

                    case 'close' :

                        City.prototype.form.close();

                        break;

                    default :

                }

            });
        });
    },

    /**
     * первичная инициализация компонентов Semantic UI
     */
    semanticInit : function()
    {
        $('.ui.dropdown').dropdown();
        $(this.element.form).form('refresh');
    },

    openLines : {

      //

    },

    calendar : {


    },

    /**
     * Установка иконки крутящихся часов на активную в текущий момент кнопку
     * @param button
     * @param action
     * @returns {boolean}
     */
    clock : function ( button = null, action = '' ) {

        if ( !button )
            button = City.prototype.buttons.currentActionButton;

        if ( !button ) return false;

        if ( !button.classList.contains(City.prototype.classes.button) ) return false;

        if ( !button.classList.contains(City.prototype.classes.clock) || action === 'show')
        {
            button.classList.add(City.prototype.classes.clock);
            button.classList.add(City.prototype.classes.disabled);
        }
        else
        {
            setTimeout( () => {
                button.classList.remove( City.prototype.classes.clock );
                button.classList.remove( City.prototype.classes.disabled );
            }, City.prototype.settings.timeOut );
        }
    },

    /**
     * Битрико - диалог - подтверждение действия
     * @param accept
     * @param titleBar
     * @param content
     * @param callback
     */
    dialog : function( accept = 'save', titleBar = 'Внимание!', content = '' , callback )
    {
        if ( !BX.PopupWindowManager ) {

            City.prototype.warning('Не удалось обнаружить метод BX.PopupWindowManager');
            City.prototype.error.critical = true;
            if ( City.prototype.checkError() ) return false;
        }

        City.prototype.element.popUpDialog = BX.PopupWindowManager.create("popup-message", null, {
            titleBar: titleBar,
            content: content,
            darkMode: false,
            autoHide: true,
            width: 500, // ширина окна
            height: 300, // высота окна
            zIndex: 100, // z-index
            closeByEsc: true, // закрытие окна по esc
            draggable: true, // можно двигать или нет
            resizable: false, // можно ресайзить
            lightShadow: false, // использовать светлую тень у окна
            angle: false, // появится уголок
            closeIcon: {opacity: 1},
            overlay: {
                backgroundColor: 'black',
                opacity: 500
            },
            buttons: [
                new BX.PopupWindowButton({
                    text: 'Да', // текст кнопки
                    //id: 'save-btn', // идентификатор
                    className: 'ui-btn ui-btn-danger-dark popup-window-button', // доп. классы
                    events: {
                        click: function() {

                            City.prototype.element.popUpDialog.close();
                            callback(true);

                        }
                    }
                }),
                new BX.PopupWindowButton({
                    text: 'Нет',
                    //id: 'copy-btn',
                    className: 'ui-btn popup-window-button',
                    events: {
                        click: function() {

                            City.prototype.element.popUpDialog.close();
                            callback(false);
                        }
                    }
                })
            ],
            events: {
                onPopupShow: function() {
                    // Событие при показе окна
                },
                onPopupClose: function() {

                    City.prototype.element.popUpDialog.destroy();
                }
            }
        });

        City.prototype.element.popUpDialog.show();
    },

    /**
     * Вывод битриксового алерта в указанном месте
     * @param parentObject
     * @param type
     * @param content
     */
    alert : function ( parentObject = null, type = 'DANGER', content = null )
    {
        if ( type === 'hide' )
        {
            BX.adjust(BX(parentObject), {
                html: ''
            });

            return ;
        }

        if ( !BX.UI.Alert ) {

            City.prototype.warning('Не удалось обнаружить метод BX.UI.Alert');
            City.prototype.error.critical = true;
            if ( City.prototype.checkError() ) return false;
        }

        let alert = new BX.UI.Alert({
            color: type === 'DANGER' ? BX.UI.Alert.Color.DANGER : BX.UI.Alert.Color.WARNING,
            icon: type === 'DANGER' ? BX.UI.Alert.Icon.DANGER : BX.UI.Alert.Icon.WARNING,
            text: content,
            closeBtn: true,
            animated : true
        });

        if ( !BX(parentObject) ) return false;

        BX.adjust(BX(parentObject), {
            html: ''
        });

        BX.append(alert.getContainer(), BX(parentObject));
    },

    /**
     * Управление выбором действий
     * @param setup
     * @returns {boolean}
     */
    action : function( setup = {} )
    {
        if ( !setup.id )
        {
            this.error.critical = true;
            this.warning('Failed to get item ID - work cannot continue');
        }

        if ( !setup.action )
        {
            this.error.critical = true;
            this.warning('The type of action is not specified, the continuation is impossible');
        }

        if ( this.checkError() ) return false;

        this.id = setup.id;

        if ( setup.action === 'edit' )
            City.prototype.form.open();


        City.prototype.server( setup );

        return true;
    },

    /**
     * Проверка ошибок
     * @returns {boolean}
     */
    checkError : function()
    {
        if ( !this.element.table || !this.element.form )
            this.error.critical = true;

        if ( this.settings.debug && this.error.warning )
            console.log( this.error.warning );

        if ( this.error.critical )
        {
            console.error('Fatal error detected, execution stopped');
            return true;
        }

        if ( !this.error.critical ) return false;
    },

    /**
     * Добавление предупреждений в список
     * @param string
     */
    warning : function( string = null )
    {
        if ( !string )
            string = 'Unknown warning';

        this.error.warning.push( string );
    },


    save : function( setup = {} ){

        City.prototype.server( setup );

    },

    form : {

        /**
         * Сабмит формы
         * @returns {boolean}
         */
        submit: function()
        {
            $(City.prototype.element.form).form('validate form');

            if ( !$(City.prototype.element.form).form('is valid') )
            {
                City.prototype.warning('Errors were found in filling out the form, data cannot be saved');

                if(City.prototype.checkError()) return false;
            }

            City.prototype.dialog( 'save', 'Внимание!', 'Вы собираетесь сохранить данные.<br>Вы уверены?', ( result ) => {

                if ( !result ) return false;
                City.prototype.save( { action: 'save' } );

            });

        },

        /**
         * Очистка данных формы
         * @returns {boolean}
         */
        reset: function()
        {
            if ( !City.prototype.element.form )
            {
                City.prototype.warning('No shape object detected');

                if(City.prototype.checkError()) return false;
            }

            City.prototype.dialog( 'reset', 'Внимание!', 'Вы собираетесь очистить данные формы.<br>Вы уверены?', ( result ) =>
            {
                if ( !result ) return false;

                City.prototype.loader('show', City.prototype.element.form);

                $(City.prototype.element.form)
                    .form('clear')
                        .form('reset')
                            .form('refresh');

                City.prototype.loader('hide', City.prototype.element.form);

            });

        },

        /**
         * Открытие основной формы
         */
        open: function()
        {
            City.prototype.fadeIn(City.prototype.element.form);
            City.prototype.fadeOut(City.prototype.element.table);
            //City.prototype.loader('show', this.element.form);
            City.prototype.form.autocomplete( City.prototype.element.form );

            City.prototype.alert( 'cityUserAssignFormAlert', 'DANGER', 'adfsfsdfsdfsdfsdf' );
        },

        /**
         * Закрытие основной формы
         */
        close: function()
        {
            // todo очистка формы

            $(City.prototype.element.form).form('clear');
            $(City.prototype.element.form).form('reset');

            City.prototype.fadeOut(City.prototype.element.form);
            City.prototype.fadeIn(City.prototype.element.table);
            City.prototype.table.reload();
        },

        /**
         * Отключаем автокомплит для полей ввода в форме
         * @param form
         */
        autocomplete: function( form )
        {
            if ( !form )
                form = this.element.form;

            Array.from( form ).map(( item )=> {

                if ( !item ) return;
                if ( item.hasAttribute('autocomplete') ) return;
                if ( item.tagName !== 'INPUT' ) return;
                item.setAttribute('autocomplete', 'nope');

            });
        }
    }
    ,

    /**
     * Эффект открытия элемента
     * @param element
     */
    fadeIn : function( element )
    {
        element.style.display = 'block';
        //$(element).transition('fade');
    },

    /**
     * Эффект закрытия элемента
     * @param element
     */
    fadeOut: function ( element )
    {
        //$(element).transition('hide');
        element.style.display = 'none';
    },

    /**
     * Добавление элемента загрузки для указанного элемента
     * UI Semantic
     * @param type
     * @param element
     */
    loader: function( type = '', element = null )
    {
        if ( !element || !type ) return false;
        if ( element.classList.contains( 'visible' ) )
            element.classList.remove( 'visible', 'transition' );

        if ( type === 'show' )
        {
            element.classList.add( this.classes.loader );

            if ( City.prototype.buttons.currentActionButton )
                City.prototype.clock(City.prototype.buttons.currentActionButton, 'show' );

        }
        else
        {
            if ( !element.classList.contains(this.classes.loader) ) return false;

            if ( City.prototype.buttons.currentActionButton )
                City.prototype.clock(City.prototype.buttons.currentActionButton, 'hide' );

            setTimeout( () => {element.classList.remove( this.classes.loader )}, City.prototype.settings.timeOut );
        }

        City.prototype.clock();
    },

    /**
     * Управление основной таблицей
     */
    table : {

      reload : function()
      {
          if (!City.prototype.element.grid.hasOwnProperty('instance')) return false;

          City.prototype.element.grid.instance
              .reloadTable('POST');
      }

    },

    /**
     * Обмен данными с сервером
     * @returns {boolean}
     */
    server : function( setup = {} )
    {
        if ( !BX )
        {
            this.error.critical = true;
            this.warning('Failed to get [BX] object to continue.');
        }

        if ( !this.settings.url )
        {
            this.error.critical = true;
            this.warning('File path not found in settings');
        }

        if ( !this.settings.file )
        {
            this.error.critical = true;
            this.warning('File name not found');
        }

        if ( this.checkError() ) return false;

        this.loader('show', City.prototype.element.form );

        BX.ajax({
            'url': this.settings.url + this.settings.file + '?action=' + setup.action,
            'method': 'post',
            'data': this.serialize(City.prototype.element.form), //$(City.prototype.element.form).serializeArray(),//
            //'dataType': 'json', //html|json|script – what type of data is expected in the response
            //'timeout': City.prototype.settings.timeOut * 5,
            'async': true,
            //'processData': true,
            //'scriptsRunFirst': true,
            //'emulateOnload': true,
            //'start': true, //|false – whether to send the request immediately or it will be launched manually
            'cache': false, //
            onsuccess: function ( data )
            {
                City.prototype.loader('hide', City.prototype.element.form );
            },
            onerror: function( data )
            {
                City.prototype.loader('hide', City.prototype.element.form );
            },
            onfailure: function( data )
            {
                City.prototype.loader('hide', City.prototype.element.form );
            }
        });
    },

    serialize : function (form)
    {
        // Setup our serialized data
        var serialized = [];

        // Loop through each field in the form
        for (var i = 0; i < form.elements.length; i++) {

            var field = form.elements[i];

            // Don't serialize fields without a name, submits, buttons, file and reset inputs, and disabled fields
            if (!field.name || field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') continue;

            // If a multi-select, get all selections
            if (field.type === 'select-multiple') {
                for (var n = 0; n < field.options.length; n++) {
                    if (!field.options[n].selected) continue;
                    serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.options[n].value));
                }
            }

            // Convert field data to a query string
            else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
                serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.value));
            }
        }

        return serialized.join('&');

    }
}