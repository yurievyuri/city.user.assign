<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

    use \Bitrix\Main\Localization\Loc;

    Loc::loadMessages(__FILE__);

    $arComponentDescription = array(
        "NAME" => 'График отвественных',
        "DESCRIPTION" => 'Управление графиков ответственных в городах и открытых линиях',
        "ICON" => "/images/icon.gif",
        "COMPLEX" => "N",
        "PATH" => array()
    );